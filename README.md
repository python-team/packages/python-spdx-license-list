<!--
SPDX-FileCopyrightText: 2019 Michael Pöhn <michael@poehn.at>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# spdx\_license\_list

A simple tool/library for working with SPDX license definitions.

The sole purpose of this module is to make SPDX definitions available in
python. Thus eliminating the chore of handling SPDX definitions and
parsing them.

This is not a SPDX specification implementation. If you're looking for
and actually standard conform implementation consult: https://spdx.org/tools

## Tests and Checks

[![SPDX definitions up to date?](https://gitlab.com/uniqx/spdx-license-list-update-check/badges/master/build.svg)](https://gitlab.com/uniqx/spdx-license-list-update-check/pipelines) SPDX definitions up to date?  
[![unit tests and code checks okay?](https://gitlab.com/uniqx/spdx-license-list/badges/master/build.svg)](https://gitlab.com/uniqx/spdx-license-list/pipelines) unit tests and code checks okay?

## Installation

The easiest way to get `spdx_license_list` is installing it using pip:

    python3 -m pip install spdx-license-list

## API

This library mostly comes with SPDX data as python dictionaries. All data
is accessible as items for our main module

    import spdx_license_list

### API for working with licenses

All license registered with SPDX are available as a huge dictionary.
This is where it's located:

    spdx_license_list.LICENSES

This dictionary uses SPDX-license IDs as key and the value is again a
dictionary containing following items. Currently only this subset of
values is available:

* __isDeprecatedLicenseId__ - bool value telling you whether this license
  definition is oudated or not.
* __isFsfLibre__ - bool value telling you whether this license is "free
  software" by the definition of FSF.
* __isOsiApproved__ - bool value telling you whether this license is
  "open source" by the definition of OSI.
* __licenseId__ - the SPDX license ID
* __name__ - the full name of this license
* __referenceNumber__ - a integer id unique for this license

### API examples

Get a names of licenses approved by FSF:

    [x['licenseId'] for x in spdx_license_list.LICENSES.values() if x['isFsfLibre']]

Get all reference numbers of non deprecated license entries:

    [x['referenceNumber'] for x in spdx_license_list.LICENSES.values() if not x['isDeprecatedLicenseId']]

Get all license IDs:

    spdx_license_list.LICENSES.values.keys()

Get reference number for each known license ID:

    {k: v['referenceNumber'] for k, v in spdx_license_list.LICENSES.items()}
    

## CLI tool

We've also included a CLI tool to do some simple operations. Try the help
option (`-h`) for getting more infos.

### CLI examples

Install on Debian/Ubuntu and get the list of standardized licenses approved either by FSF or OSI:

    sudo apt install python3-pip
    python3 -m pip install spdx-license-list
    python3 -m spdx_license_list --filter-fsf-or-osi

Print a list containing all SPDX standardized licneses approved by both FSF and OSI:

    python3 -m spdx_license_list print --filter-fsf --filter-osi

Check if this version on `spdx_license_list` sill is up to date, or if there's a newer release
of SPDX license definitions available.

    python3 -m spdx_license_list check-version
