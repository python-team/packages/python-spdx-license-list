#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2019 Michael Pöhn <michael@poehn.at>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import spdx_license_list
import setuptools


with open('README.md', 'r') as f:
    long_description = f.read()


d = {'name': spdx_license_list.NAME,
     'version': spdx_license_list.VERSION,
     'author': 'unique entity',
     'author_email': 'uniq@h4x.at',
     'description': 'A simple tool/library for working with SPDX license definitions.',
     'long_description': long_description,
     'long_description_content_type': 'text/markdown',
     'url': 'https://gitlab.com/uniqx/spdx_license_list',
     'packages': ['spdx_license_list'],
     'classifiers': ['Programming Language :: Python :: 3',
                     'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
                     'Operating System :: OS Independent']}


setuptools.setup(**d)
