#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2019 Michael Pöhn <michael@poehn.at>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# this file is generated using `python -m spdx_license_list gen-py`
# do not modify it manually

NAME = 'spdx_license_list'

VERSION = '0.5.2'

SPDX_GIT_TAG = 'v3.11'

LICENSES = {
    '0BSD': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': '0BSD',
        'name': 'BSD Zero Clause License',
        'referenceNumber': 244
    },
    'AAL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'AAL',
        'name': 'Attribution Assurance License',
        'referenceNumber': 59
    },
    'ADSL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'ADSL',
        'name': 'Amazon Digital Services License',
        'referenceNumber': 223
    },
    'AFL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'AFL-1.1',
        'name': 'Academic Free License v1.1',
        'referenceNumber': 142
    },
    'AFL-1.2': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'AFL-1.2',
        'name': 'Academic Free License v1.2',
        'referenceNumber': 255
    },
    'AFL-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'AFL-2.0',
        'name': 'Academic Free License v2.0',
        'referenceNumber': 447
    },
    'AFL-2.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'AFL-2.1',
        'name': 'Academic Free License v2.1',
        'referenceNumber': 258
    },
    'AFL-3.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'AFL-3.0',
        'name': 'Academic Free License v3.0',
        'referenceNumber': 391
    },
    'AGPL-1.0': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'AGPL-1.0',
        'name': 'Affero General Public License v1.0',
        'referenceNumber': 174
    },
    'AGPL-1.0-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'AGPL-1.0-only',
        'name': 'Affero General Public License v1.0 only',
        'referenceNumber': 72
    },
    'AGPL-1.0-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'AGPL-1.0-or-later',
        'name': 'Affero General Public License v1.0 or later',
        'referenceNumber': 167
    },
    'AGPL-3.0': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'AGPL-3.0',
        'name': 'GNU Affero General Public License v3.0',
        'referenceNumber': 145
    },
    'AGPL-3.0-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'AGPL-3.0-only',
        'name': 'GNU Affero General Public License v3.0 only',
        'referenceNumber': 312
    },
    'AGPL-3.0-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'AGPL-3.0-or-later',
        'name': 'GNU Affero General Public License v3.0 or '
        'later',
        'referenceNumber': 160
    },
    'AMDPLPA': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'AMDPLPA',
        'name': "AMD's plpa_map.c License",
        'referenceNumber': 132
    },
    'AML': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'AML',
        'name': 'Apple MIT License',
        'referenceNumber': 155
    },
    'AMPAS': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'AMPAS',
        'name': 'Academy of Motion Picture Arts and Sciences BSD',
        'referenceNumber': 133
    },
    'ANTLR-PD': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'ANTLR-PD',
        'name': 'ANTLR Software Rights Notice',
        'referenceNumber': 40
    },
    'ANTLR-PD-fallback': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'ANTLR-PD-fallback',
        'name': 'ANTLR Software Rights Notice with license '
        'fallback',
        'referenceNumber': 153
    },
    'APAFML': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'APAFML',
        'name': 'Adobe Postscript AFM License',
        'referenceNumber': 254
    },
    'APL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'APL-1.0',
        'name': 'Adaptive Public License 1.0',
        'referenceNumber': 284
    },
    'APSL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'APSL-1.0',
        'name': 'Apple Public Source License 1.0',
        'referenceNumber': 403
    },
    'APSL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'APSL-1.1',
        'name': 'Apple Public Source License 1.1',
        'referenceNumber': 446
    },
    'APSL-1.2': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'APSL-1.2',
        'name': 'Apple Public Source License 1.2',
        'referenceNumber': 205
    },
    'APSL-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'APSL-2.0',
        'name': 'Apple Public Source License 2.0',
        'referenceNumber': 204
    },
    'Abstyles': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Abstyles',
        'name': 'Abstyles License',
        'referenceNumber': 92
    },
    'Adobe-2006': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Adobe-2006',
        'name': 'Adobe Systems Incorporated Source Code License '
        'Agreement',
        'referenceNumber': 355
    },
    'Adobe-Glyph': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Adobe-Glyph',
        'name': 'Adobe Glyph List License',
        'referenceNumber': 357
    },
    'Afmparse': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Afmparse',
        'name': 'Afmparse License',
        'referenceNumber': 346
    },
    'Aladdin': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Aladdin',
        'name': 'Aladdin Free Public License',
        'referenceNumber': 336
    },
    'Apache-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'Apache-1.0',
        'name': 'Apache License 1.0',
        'referenceNumber': 143
    },
    'Apache-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'Apache-1.1',
        'name': 'Apache License 1.1',
        'referenceNumber': 292
    },
    'Apache-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'Apache-2.0',
        'name': 'Apache License 2.0',
        'referenceNumber': 383
    },
    'Artistic-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'Artistic-1.0',
        'name': 'Artistic License 1.0',
        'referenceNumber': 279
    },
    'Artistic-1.0-Perl': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'Artistic-1.0-Perl',
        'name': 'Artistic License 1.0 (Perl)',
        'referenceNumber': 318
    },
    'Artistic-1.0-cl8': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'Artistic-1.0-cl8',
        'name': 'Artistic License 1.0 w/clause 8',
        'referenceNumber': 234
    },
    'Artistic-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'Artistic-2.0',
        'name': 'Artistic License 2.0',
        'referenceNumber': 80
    },
    'BSD-1-Clause': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'BSD-1-Clause',
        'name': 'BSD 1-Clause License',
        'referenceNumber': 405
    },
    'BSD-2-Clause': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'BSD-2-Clause',
        'name': 'BSD 2-Clause "Simplified" License',
        'referenceNumber': 317
    },
    'BSD-2-Clause-FreeBSD': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'BSD-2-Clause-FreeBSD',
        'name': 'BSD 2-Clause FreeBSD License',
        'referenceNumber': 288
    },
    'BSD-2-Clause-NetBSD': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BSD-2-Clause-NetBSD',
        'name': 'BSD 2-Clause NetBSD License',
        'referenceNumber': 187
    },
    'BSD-2-Clause-Patent': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'BSD-2-Clause-Patent',
        'name': 'BSD-2-Clause Plus Patent License',
        'referenceNumber': 380
    },
    'BSD-2-Clause-Views': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BSD-2-Clause-Views',
        'name': 'BSD 2-Clause with views sentence',
        'referenceNumber': 285
    },
    'BSD-3-Clause': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'BSD-3-Clause',
        'name': 'BSD 3-Clause "New" or "Revised" License',
        'referenceNumber': 207
    },
    'BSD-3-Clause-Attribution': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BSD-3-Clause-Attribution',
        'name': 'BSD with attribution',
        'referenceNumber': 35
    },
    'BSD-3-Clause-Clear': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'BSD-3-Clause-Clear',
        'name': 'BSD 3-Clause Clear License',
        'referenceNumber': 85
    },
    'BSD-3-Clause-LBNL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'BSD-3-Clause-LBNL',
        'name': 'Lawrence Berkeley National Labs BSD variant '
        'license',
        'referenceNumber': 144
    },
    'BSD-3-Clause-No-Nuclear-License': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BSD-3-Clause-No-Nuclear-License',
        'name': 'BSD 3-Clause No Nuclear License',
        'referenceNumber': 179
    },
    'BSD-3-Clause-No-Nuclear-License-2014': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BSD-3-Clause-No-Nuclear-License-2014',
        'name': 'BSD 3-Clause No Nuclear '
        'License 2014',
        'referenceNumber': 356
    },
    'BSD-3-Clause-No-Nuclear-Warranty': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BSD-3-Clause-No-Nuclear-Warranty',
        'name': 'BSD 3-Clause No Nuclear '
        'Warranty',
        'referenceNumber': 116
    },
    'BSD-3-Clause-Open-MPI': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BSD-3-Clause-Open-MPI',
        'name': 'BSD 3-Clause Open MPI variant',
        'referenceNumber': 218
    },
    'BSD-4-Clause': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'BSD-4-Clause',
        'name': 'BSD 4-Clause "Original" or "Old" License',
        'referenceNumber': 66
    },
    'BSD-4-Clause-UC': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BSD-4-Clause-UC',
        'name': 'BSD-4-Clause (University of California-Specific)',
        'referenceNumber': 390
    },
    'BSD-Protection': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BSD-Protection',
        'name': 'BSD Protection License',
        'referenceNumber': 418
    },
    'BSD-Source-Code': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BSD-Source-Code',
        'name': 'BSD Source Code Attribution',
        'referenceNumber': 168
    },
    'BSL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'BSL-1.0',
        'name': 'Boost Software License 1.0',
        'referenceNumber': 303
    },
    'BUSL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BUSL-1.1',
        'name': 'Business Source License 1.1',
        'referenceNumber': 38
    },
    'Bahyph': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Bahyph',
        'name': 'Bahyph License',
        'referenceNumber': 148
    },
    'Barr': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Barr',
        'name': 'Barr License',
        'referenceNumber': 125
    },
    'Beerware': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Beerware',
        'name': 'Beerware License',
        'referenceNumber': 257
    },
    'BitTorrent-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BitTorrent-1.0',
        'name': 'BitTorrent Open Source License v1.0',
        'referenceNumber': 211
    },
    'BitTorrent-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'BitTorrent-1.1',
        'name': 'BitTorrent Open Source License v1.1',
        'referenceNumber': 190
    },
    'BlueOak-1.0.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'BlueOak-1.0.0',
        'name': 'Blue Oak Model License 1.0.0',
        'referenceNumber': 216
    },
    'Borceux': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Borceux',
        'name': 'Borceux license',
        'referenceNumber': 316
    },
    'CAL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'CAL-1.0',
        'name': 'Cryptographic Autonomy License 1.0',
        'referenceNumber': 55
    },
    'CAL-1.0-Combined-Work-Exception': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'CAL-1.0-Combined-Work-Exception',
        'name': 'Cryptographic Autonomy License '
        '1.0 (Combined Work Exception)',
        'referenceNumber': 76
    },
    'CATOSL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'CATOSL-1.1',
        'name': 'Computer Associates Trusted Open Source License 1.1',
        'referenceNumber': 243
    },
    'CC-BY-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-1.0',
        'name': 'Creative Commons Attribution 1.0 Generic',
        'referenceNumber': 16
    },
    'CC-BY-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-2.0',
        'name': 'Creative Commons Attribution 2.0 Generic',
        'referenceNumber': 57
    },
    'CC-BY-2.5': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-2.5',
        'name': 'Creative Commons Attribution 2.5 Generic',
        'referenceNumber': 192
    },
    'CC-BY-3.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-3.0',
        'name': 'Creative Commons Attribution 3.0 Unported',
        'referenceNumber': 358
    },
    'CC-BY-3.0-AT': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-3.0-AT',
        'name': 'Creative Commons Attribution 3.0 Austria',
        'referenceNumber': 440
    },
    'CC-BY-3.0-US': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-3.0-US',
        'name': 'Creative Commons Attribution 3.0 United States',
        'referenceNumber': 334
    },
    'CC-BY-4.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-4.0',
        'name': 'Creative Commons Attribution 4.0 International',
        'referenceNumber': 224
    },
    'CC-BY-NC-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-1.0',
        'name': 'Creative Commons Attribution Non Commercial 1.0 '
        'Generic',
        'referenceNumber': 239
    },
    'CC-BY-NC-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-2.0',
        'name': 'Creative Commons Attribution Non Commercial 2.0 '
        'Generic',
        'referenceNumber': 339
    },
    'CC-BY-NC-2.5': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-2.5',
        'name': 'Creative Commons Attribution Non Commercial 2.5 '
        'Generic',
        'referenceNumber': 414
    },
    'CC-BY-NC-3.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-3.0',
        'name': 'Creative Commons Attribution Non Commercial 3.0 '
        'Unported',
        'referenceNumber': 349
    },
    'CC-BY-NC-4.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-4.0',
        'name': 'Creative Commons Attribution Non Commercial 4.0 '
        'International',
        'referenceNumber': 278
    },
    'CC-BY-NC-ND-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-ND-1.0',
        'name': 'Creative Commons Attribution Non Commercial No '
        'Derivatives 1.0 Generic',
        'referenceNumber': 99
    },
    'CC-BY-NC-ND-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-ND-2.0',
        'name': 'Creative Commons Attribution Non Commercial No '
        'Derivatives 2.0 Generic',
        'referenceNumber': 135
    },
    'CC-BY-NC-ND-2.5': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-ND-2.5',
        'name': 'Creative Commons Attribution Non Commercial No '
        'Derivatives 2.5 Generic',
        'referenceNumber': 23
    },
    'CC-BY-NC-ND-3.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-ND-3.0',
        'name': 'Creative Commons Attribution Non Commercial No '
        'Derivatives 3.0 Unported',
        'referenceNumber': 36
    },
    'CC-BY-NC-ND-3.0-IGO': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-ND-3.0-IGO',
        'name': 'Creative Commons Attribution Non Commercial '
        'No Derivatives 3.0 IGO',
        'referenceNumber': 363
    },
    'CC-BY-NC-ND-4.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-ND-4.0',
        'name': 'Creative Commons Attribution Non Commercial No '
        'Derivatives 4.0 International',
        'referenceNumber': 171
    },
    'CC-BY-NC-SA-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-SA-1.0',
        'name': 'Creative Commons Attribution Non Commercial '
        'Share Alike 1.0 Generic',
        'referenceNumber': 117
    },
    'CC-BY-NC-SA-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-SA-2.0',
        'name': 'Creative Commons Attribution Non Commercial '
        'Share Alike 2.0 Generic',
        'referenceNumber': 428
    },
    'CC-BY-NC-SA-2.5': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-SA-2.5',
        'name': 'Creative Commons Attribution Non Commercial '
        'Share Alike 2.5 Generic',
        'referenceNumber': 222
    },
    'CC-BY-NC-SA-3.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-SA-3.0',
        'name': 'Creative Commons Attribution Non Commercial '
        'Share Alike 3.0 Unported',
        'referenceNumber': 376
    },
    'CC-BY-NC-SA-4.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-NC-SA-4.0',
        'name': 'Creative Commons Attribution Non Commercial '
        'Share Alike 4.0 International',
        'referenceNumber': 345
    },
    'CC-BY-ND-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-ND-1.0',
        'name': 'Creative Commons Attribution No Derivatives 1.0 '
        'Generic',
        'referenceNumber': 91
    },
    'CC-BY-ND-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-ND-2.0',
        'name': 'Creative Commons Attribution No Derivatives 2.0 '
        'Generic',
        'referenceNumber': 50
    },
    'CC-BY-ND-2.5': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-ND-2.5',
        'name': 'Creative Commons Attribution No Derivatives 2.5 '
        'Generic',
        'referenceNumber': 22
    },
    'CC-BY-ND-3.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-ND-3.0',
        'name': 'Creative Commons Attribution No Derivatives 3.0 '
        'Unported',
        'referenceNumber': 295
    },
    'CC-BY-ND-4.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-ND-4.0',
        'name': 'Creative Commons Attribution No Derivatives 4.0 '
        'International',
        'referenceNumber': 337
    },
    'CC-BY-SA-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-SA-1.0',
        'name': 'Creative Commons Attribution Share Alike 1.0 '
        'Generic',
        'referenceNumber': 419
    },
    'CC-BY-SA-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-SA-2.0',
        'name': 'Creative Commons Attribution Share Alike 2.0 '
        'Generic',
        'referenceNumber': 386
    },
    'CC-BY-SA-2.0-UK': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-SA-2.0-UK',
        'name': 'Creative Commons Attribution Share Alike 2.0 '
        'England and Wales',
        'referenceNumber': 5
    },
    'CC-BY-SA-2.5': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-SA-2.5',
        'name': 'Creative Commons Attribution Share Alike 2.5 '
        'Generic',
        'referenceNumber': 209
    },
    'CC-BY-SA-3.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-SA-3.0',
        'name': 'Creative Commons Attribution Share Alike 3.0 '
        'Unported',
        'referenceNumber': 238
    },
    'CC-BY-SA-3.0-AT': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-SA-3.0-AT',
        'name': 'Creative Commons Attribution-Share Alike 3.0 '
        'Austria',
        'referenceNumber': 299
    },
    'CC-BY-SA-4.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'CC-BY-SA-4.0',
        'name': 'Creative Commons Attribution Share Alike 4.0 '
        'International',
        'referenceNumber': 333
    },
    'CC-PDDC': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CC-PDDC',
        'name': 'Creative Commons Public Domain Dedication and '
        'Certification',
        'referenceNumber': 90
    },
    'CC0-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'CC0-1.0',
        'name': 'Creative Commons Zero v1.0 Universal',
        'referenceNumber': 63
    },
    'CDDL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'CDDL-1.0',
        'name': 'Common Development and Distribution License 1.0',
        'referenceNumber': 348
    },
    'CDDL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CDDL-1.1',
        'name': 'Common Development and Distribution License 1.1',
        'referenceNumber': 443
    },
    'CDLA-Permissive-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CDLA-Permissive-1.0',
        'name': 'Community Data License Agreement Permissive '
        '1.0',
        'referenceNumber': 107
    },
    'CDLA-Sharing-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CDLA-Sharing-1.0',
        'name': 'Community Data License Agreement Sharing 1.0',
        'referenceNumber': 178
    },
    'CECILL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CECILL-1.0',
        'name': 'CeCILL Free Software License Agreement v1.0',
        'referenceNumber': 64
    },
    'CECILL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CECILL-1.1',
        'name': 'CeCILL Free Software License Agreement v1.1',
        'referenceNumber': 161
    },
    'CECILL-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'CECILL-2.0',
        'name': 'CeCILL Free Software License Agreement v2.0',
        'referenceNumber': 61
    },
    'CECILL-2.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'CECILL-2.1',
        'name': 'CeCILL Free Software License Agreement v2.1',
        'referenceNumber': 173
    },
    'CECILL-B': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'CECILL-B',
        'name': 'CeCILL-B Free Software License Agreement',
        'referenceNumber': 137
    },
    'CECILL-C': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'CECILL-C',
        'name': 'CeCILL-C Free Software License Agreement',
        'referenceNumber': 269
    },
    'CERN-OHL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CERN-OHL-1.1',
        'name': 'CERN Open Hardware Licence v1.1',
        'referenceNumber': 122
    },
    'CERN-OHL-1.2': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CERN-OHL-1.2',
        'name': 'CERN Open Hardware Licence v1.2',
        'referenceNumber': 169
    },
    'CERN-OHL-P-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CERN-OHL-P-2.0',
        'name': 'CERN Open Hardware Licence Version 2 - Permissive',
        'referenceNumber': 277
    },
    'CERN-OHL-S-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CERN-OHL-S-2.0',
        'name': 'CERN Open Hardware Licence Version 2 - Strongly '
        'Reciprocal',
        'referenceNumber': 51
    },
    'CERN-OHL-W-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CERN-OHL-W-2.0',
        'name': 'CERN Open Hardware Licence Version 2 - Weakly '
        'Reciprocal',
        'referenceNumber': 274
    },
    'CNRI-Jython': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CNRI-Jython',
        'name': 'CNRI Jython License',
        'referenceNumber': 73
    },
    'CNRI-Python': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'CNRI-Python',
        'name': 'CNRI Python License',
        'referenceNumber': 95
    },
    'CNRI-Python-GPL-Compatible': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CNRI-Python-GPL-Compatible',
        'name': 'CNRI Python Open Source GPL '
        'Compatible License Agreement',
        'referenceNumber': 372
    },
    'CPAL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'CPAL-1.0',
        'name': 'Common Public Attribution License 1.0',
        'referenceNumber': 293
    },
    'CPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'CPL-1.0',
        'name': 'Common Public License 1.0',
        'referenceNumber': 241
    },
    'CPOL-1.02': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CPOL-1.02',
        'name': 'Code Project Open License 1.02',
        'referenceNumber': 236
    },
    'CUA-OPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'CUA-OPL-1.0',
        'name': 'CUA Office Public License v1.0',
        'referenceNumber': 165
    },
    'Caldera': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Caldera',
        'name': 'Caldera License',
        'referenceNumber': 256
    },
    'ClArtistic': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'ClArtistic',
        'name': 'Clarified Artistic License',
        'referenceNumber': 251
    },
    'Condor-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'Condor-1.1',
        'name': 'Condor Public License v1.1',
        'referenceNumber': 151
    },
    'Crossword': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Crossword',
        'name': 'Crossword License',
        'referenceNumber': 98
    },
    'CrystalStacker': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'CrystalStacker',
        'name': 'CrystalStacker License',
        'referenceNumber': 33
    },
    'Cube': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Cube',
        'name': 'Cube License',
        'referenceNumber': 407
    },
    'D-FSL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'D-FSL-1.0',
        'name': 'Deutsche Freie Software Lizenz',
        'referenceNumber': 378
    },
    'DOC': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'DOC',
        'name': 'DOC License',
        'referenceNumber': 281
    },
    'DSDP': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'DSDP',
        'name': 'DSDP License',
        'referenceNumber': 272
    },
    'Dotseqn': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Dotseqn',
        'name': 'Dotseqn License',
        'referenceNumber': 26
    },
    'ECL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'ECL-1.0',
        'name': 'Educational Community License v1.0',
        'referenceNumber': 439
    },
    'ECL-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'ECL-2.0',
        'name': 'Educational Community License v2.0',
        'referenceNumber': 8
    },
    'EFL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'EFL-1.0',
        'name': 'Eiffel Forum License v1.0',
        'referenceNumber': 75
    },
    'EFL-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'EFL-2.0',
        'name': 'Eiffel Forum License v2.0',
        'referenceNumber': 7
    },
    'EPICS': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'EPICS',
        'name': 'EPICS Open License',
        'referenceNumber': 374
    },
    'EPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'EPL-1.0',
        'name': 'Eclipse Public License 1.0',
        'referenceNumber': 289
    },
    'EPL-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'EPL-2.0',
        'name': 'Eclipse Public License 2.0',
        'referenceNumber': 434
    },
    'EUDatagrid': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'EUDatagrid',
        'name': 'EU DataGrid Software License',
        'referenceNumber': 270
    },
    'EUPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'EUPL-1.0',
        'name': 'European Union Public License 1.0',
        'referenceNumber': 100
    },
    'EUPL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'EUPL-1.1',
        'name': 'European Union Public License 1.1',
        'referenceNumber': 426
    },
    'EUPL-1.2': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'EUPL-1.2',
        'name': 'European Union Public License 1.2',
        'referenceNumber': 280
    },
    'Entessa': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'Entessa',
        'name': 'Entessa Public License v1.0',
        'referenceNumber': 402
    },
    'ErlPL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'ErlPL-1.1',
        'name': 'Erlang Public License v1.1',
        'referenceNumber': 424
    },
    'Eurosym': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Eurosym',
        'name': 'Eurosym License',
        'referenceNumber': 170
    },
    'FSFAP': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'FSFAP',
        'name': 'FSF All Permissive License',
        'referenceNumber': 410
    },
    'FSFUL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'FSFUL',
        'name': 'FSF Unlimited License',
        'referenceNumber': 2
    },
    'FSFULLR': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'FSFULLR',
        'name': 'FSF Unlimited License (with License Retention)',
        'referenceNumber': 319
    },
    'FTL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'FTL',
        'name': 'Freetype Project License',
        'referenceNumber': 448
    },
    'Fair': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'Fair',
        'name': 'Fair License',
        'referenceNumber': 268
    },
    'Frameworx-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'Frameworx-1.0',
        'name': 'Frameworx Open License 1.0',
        'referenceNumber': 389
    },
    'FreeImage': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'FreeImage',
        'name': 'FreeImage Public License v1.0',
        'referenceNumber': 388
    },
    'GFDL-1.1': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.1',
        'name': 'GNU Free Documentation License v1.1',
        'referenceNumber': 291
    },
    'GFDL-1.1-invariants-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.1-invariants-only',
        'name': 'GNU Free Documentation License v1.1 '
        'only - invariants',
        'referenceNumber': 200
    },
    'GFDL-1.1-invariants-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.1-invariants-or-later',
        'name': 'GNU Free Documentation License v1.1 '
        'or later - invariants',
        'referenceNumber': 78
    },
    'GFDL-1.1-no-invariants-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.1-no-invariants-only',
        'name': 'GNU Free Documentation License v1.1 '
        'only - no invariants',
        'referenceNumber': 13
    },
    'GFDL-1.1-no-invariants-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.1-no-invariants-or-later',
        'name': 'GNU Free Documentation License '
        'v1.1 or later - no invariants',
        'referenceNumber': 248
    },
    'GFDL-1.1-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.1-only',
        'name': 'GNU Free Documentation License v1.1 only',
        'referenceNumber': 101
    },
    'GFDL-1.1-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.1-or-later',
        'name': 'GNU Free Documentation License v1.1 or later',
        'referenceNumber': 121
    },
    'GFDL-1.2': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.2',
        'name': 'GNU Free Documentation License v1.2',
        'referenceNumber': 324
    },
    'GFDL-1.2-invariants-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.2-invariants-only',
        'name': 'GNU Free Documentation License v1.2 '
        'only - invariants',
        'referenceNumber': 202
    },
    'GFDL-1.2-invariants-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.2-invariants-or-later',
        'name': 'GNU Free Documentation License v1.2 '
        'or later - invariants',
        'referenceNumber': 425
    },
    'GFDL-1.2-no-invariants-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.2-no-invariants-only',
        'name': 'GNU Free Documentation License v1.2 '
        'only - no invariants',
        'referenceNumber': 320
    },
    'GFDL-1.2-no-invariants-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.2-no-invariants-or-later',
        'name': 'GNU Free Documentation License '
        'v1.2 or later - no invariants',
        'referenceNumber': 189
    },
    'GFDL-1.2-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.2-only',
        'name': 'GNU Free Documentation License v1.2 only',
        'referenceNumber': 88
    },
    'GFDL-1.2-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.2-or-later',
        'name': 'GNU Free Documentation License v1.2 or later',
        'referenceNumber': 129
    },
    'GFDL-1.3': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.3',
        'name': 'GNU Free Documentation License v1.3',
        'referenceNumber': 381
    },
    'GFDL-1.3-invariants-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.3-invariants-only',
        'name': 'GNU Free Documentation License v1.3 '
        'only - invariants',
        'referenceNumber': 181
    },
    'GFDL-1.3-invariants-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.3-invariants-or-later',
        'name': 'GNU Free Documentation License v1.3 '
        'or later - invariants',
        'referenceNumber': 313
    },
    'GFDL-1.3-no-invariants-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.3-no-invariants-only',
        'name': 'GNU Free Documentation License v1.3 '
        'only - no invariants',
        'referenceNumber': 177
    },
    'GFDL-1.3-no-invariants-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.3-no-invariants-or-later',
        'name': 'GNU Free Documentation License '
        'v1.3 or later - no invariants',
        'referenceNumber': 162
    },
    'GFDL-1.3-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.3-only',
        'name': 'GNU Free Documentation License v1.3 only',
        'referenceNumber': 325
    },
    'GFDL-1.3-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'GFDL-1.3-or-later',
        'name': 'GNU Free Documentation License v1.3 or later',
        'referenceNumber': 49
    },
    'GL2PS': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GL2PS',
        'name': 'GL2PS License',
        'referenceNumber': 330
    },
    'GLWTPL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GLWTPL',
        'name': 'Good Luck With That Public License',
        'referenceNumber': 48
    },
    'GPL-1.0': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GPL-1.0',
        'name': 'GNU General Public License v1.0 only',
        'referenceNumber': 343
    },
    'GPL-1.0+': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GPL-1.0+',
        'name': 'GNU General Public License v1.0 or later',
        'referenceNumber': 221
    },
    'GPL-1.0-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GPL-1.0-only',
        'name': 'GNU General Public License v1.0 only',
        'referenceNumber': 10
    },
    'GPL-1.0-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GPL-1.0-or-later',
        'name': 'GNU General Public License v1.0 or later',
        'referenceNumber': 131
    },
    'GPL-2.0': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'GPL-2.0',
        'name': 'GNU General Public License v2.0 only',
        'referenceNumber': 371
    },
    'GPL-2.0+': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'GPL-2.0+',
        'name': 'GNU General Public License v2.0 or later',
        'referenceNumber': 417
    },
    'GPL-2.0-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'GPL-2.0-only',
        'name': 'GNU General Public License v2.0 only',
        'referenceNumber': 240
    },
    'GPL-2.0-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'GPL-2.0-or-later',
        'name': 'GNU General Public License v2.0 or later',
        'referenceNumber': 264
    },
    'GPL-2.0-with-GCC-exception': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GPL-2.0-with-GCC-exception',
        'name': 'GNU General Public License v2.0 w/GCC '
        'Runtime Library exception',
        'referenceNumber': 354
    },
    'GPL-2.0-with-autoconf-exception': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GPL-2.0-with-autoconf-exception',
        'name': 'GNU General Public License v2.0 '
        'w/Autoconf exception',
        'referenceNumber': 29
    },
    'GPL-2.0-with-bison-exception': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GPL-2.0-with-bison-exception',
        'name': 'GNU General Public License v2.0 '
        'w/Bison exception',
        'referenceNumber': 387
    },
    'GPL-2.0-with-classpath-exception': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GPL-2.0-with-classpath-exception',
        'name': 'GNU General Public License v2.0 '
        'w/Classpath exception',
        'referenceNumber': 232
    },
    'GPL-2.0-with-font-exception': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GPL-2.0-with-font-exception',
        'name': 'GNU General Public License v2.0 '
        'w/Font exception',
        'referenceNumber': 18
    },
    'GPL-3.0': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'GPL-3.0',
        'name': 'GNU General Public License v3.0 only',
        'referenceNumber': 431
    },
    'GPL-3.0+': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'GPL-3.0+',
        'name': 'GNU General Public License v3.0 or later',
        'referenceNumber': 149
    },
    'GPL-3.0-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'GPL-3.0-only',
        'name': 'GNU General Public License v3.0 only',
        'referenceNumber': 124
    },
    'GPL-3.0-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'GPL-3.0-or-later',
        'name': 'GNU General Public License v3.0 or later',
        'referenceNumber': 415
    },
    'GPL-3.0-with-GCC-exception': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'GPL-3.0-with-GCC-exception',
        'name': 'GNU General Public License v3.0 w/GCC '
        'Runtime Library exception',
        'referenceNumber': 1
    },
    'GPL-3.0-with-autoconf-exception': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'GPL-3.0-with-autoconf-exception',
        'name': 'GNU General Public License v3.0 '
        'w/Autoconf exception',
        'referenceNumber': 6
    },
    'Giftware': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Giftware',
        'name': 'Giftware License',
        'referenceNumber': 395
    },
    'Glide': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Glide',
        'name': '3dfx Glide License',
        'referenceNumber': 118
    },
    'Glulxe': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Glulxe',
        'name': 'Glulxe License',
        'referenceNumber': 210
    },
    'HPND': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'HPND',
        'name': 'Historical Permission Notice and Disclaimer',
        'referenceNumber': 172
    },
    'HPND-sell-variant': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'HPND-sell-variant',
        'name': 'Historical Permission Notice and Disclaimer - '
        'sell variant',
        'referenceNumber': 163
    },
    'HTMLTIDY': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'HTMLTIDY',
        'name': 'HTML Tidy License',
        'referenceNumber': 352
    },
    'HaskellReport': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'HaskellReport',
        'name': 'Haskell Language Report License',
        'referenceNumber': 219
    },
    'Hippocratic-2.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Hippocratic-2.1',
        'name': 'Hippocratic License 2.1',
        'referenceNumber': 212
    },
    'IBM-pibs': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'IBM-pibs',
        'name': 'IBM PowerPC Initialization and Boot Software',
        'referenceNumber': 331
    },
    'ICU': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'ICU',
        'name': 'ICU License',
        'referenceNumber': 182
    },
    'IJG': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'IJG',
        'name': 'Independent JPEG Group License',
        'referenceNumber': 250
    },
    'IPA': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'IPA',
        'name': 'IPA Font License',
        'referenceNumber': 341
    },
    'IPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'IPL-1.0',
        'name': 'IBM Public License v1.0',
        'referenceNumber': 445
    },
    'ISC': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'ISC',
        'name': 'ISC License',
        'referenceNumber': 382
    },
    'ImageMagick': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'ImageMagick',
        'name': 'ImageMagick License',
        'referenceNumber': 351
    },
    'Imlib2': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'Imlib2',
        'name': 'Imlib2 License',
        'referenceNumber': 138
    },
    'Info-ZIP': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Info-ZIP',
        'name': 'Info-ZIP License',
        'referenceNumber': 305
    },
    'Intel': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'Intel',
        'name': 'Intel Open Source License',
        'referenceNumber': 27
    },
    'Intel-ACPI': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Intel-ACPI',
        'name': 'Intel ACPI Software License Agreement',
        'referenceNumber': 249
    },
    'Interbase-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Interbase-1.0',
        'name': 'Interbase Public License v1.0',
        'referenceNumber': 350
    },
    'JPNIC': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'JPNIC',
        'name': 'Japan Network Information Center License',
        'referenceNumber': 340
    },
    'JSON': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'JSON',
        'name': 'JSON License',
        'referenceNumber': 208
    },
    'JasPer-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'JasPer-2.0',
        'name': 'JasPer License',
        'referenceNumber': 77
    },
    'LAL-1.2': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'LAL-1.2',
        'name': 'Licence Art Libre 1.2',
        'referenceNumber': 158
    },
    'LAL-1.3': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'LAL-1.3',
        'name': 'Licence Art Libre 1.3',
        'referenceNumber': 384
    },
    'LGPL-2.0': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'LGPL-2.0',
        'name': 'GNU Library General Public License v2 only',
        'referenceNumber': 297
    },
    'LGPL-2.0+': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'LGPL-2.0+',
        'name': 'GNU Library General Public License v2 or later',
        'referenceNumber': 141
    },
    'LGPL-2.0-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'LGPL-2.0-only',
        'name': 'GNU Library General Public License v2 only',
        'referenceNumber': 353
    },
    'LGPL-2.0-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'LGPL-2.0-or-later',
        'name': 'GNU Library General Public License v2 or later',
        'referenceNumber': 30
    },
    'LGPL-2.1': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'LGPL-2.1',
        'name': 'GNU Lesser General Public License v2.1 only',
        'referenceNumber': 191
    },
    'LGPL-2.1+': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'LGPL-2.1+',
        'name': 'GNU Library General Public License v2.1 or later',
        'referenceNumber': 213
    },
    'LGPL-2.1-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'LGPL-2.1-only',
        'name': 'GNU Lesser General Public License v2.1 only',
        'referenceNumber': 140
    },
    'LGPL-2.1-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'LGPL-2.1-or-later',
        'name': 'GNU Lesser General Public License v2.1 or '
        'later',
        'referenceNumber': 286
    },
    'LGPL-3.0': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'LGPL-3.0',
        'name': 'GNU Lesser General Public License v3.0 only',
        'referenceNumber': 220
    },
    'LGPL-3.0+': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'LGPL-3.0+',
        'name': 'GNU Lesser General Public License v3.0 or later',
        'referenceNumber': 235
    },
    'LGPL-3.0-only': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'LGPL-3.0-only',
        'name': 'GNU Lesser General Public License v3.0 only',
        'referenceNumber': 47
    },
    'LGPL-3.0-or-later': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'LGPL-3.0-or-later',
        'name': 'GNU Lesser General Public License v3.0 or '
        'later',
        'referenceNumber': 365
    },
    'LGPLLR': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'LGPLLR',
        'name': 'Lesser General Public License For Linguistic Resources',
        'referenceNumber': 435
    },
    'LPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'LPL-1.0',
        'name': 'Lucent Public License Version 1.0',
        'referenceNumber': 399
    },
    'LPL-1.02': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'LPL-1.02',
        'name': 'Lucent Public License v1.02',
        'referenceNumber': 123
    },
    'LPPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'LPPL-1.0',
        'name': 'LaTeX Project Public License v1.0',
        'referenceNumber': 86
    },
    'LPPL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'LPPL-1.1',
        'name': 'LaTeX Project Public License v1.1',
        'referenceNumber': 175
    },
    'LPPL-1.2': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'LPPL-1.2',
        'name': 'LaTeX Project Public License v1.2',
        'referenceNumber': 166
    },
    'LPPL-1.3a': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'LPPL-1.3a',
        'name': 'LaTeX Project Public License v1.3a',
        'referenceNumber': 287
    },
    'LPPL-1.3c': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'LPPL-1.3c',
        'name': 'LaTeX Project Public License v1.3c',
        'referenceNumber': 128
    },
    'Latex2e': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Latex2e',
        'name': 'Latex2e License',
        'referenceNumber': 34
    },
    'Leptonica': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Leptonica',
        'name': 'Leptonica License',
        'referenceNumber': 322
    },
    'LiLiQ-P-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'LiLiQ-P-1.1',
        'name': 'Licence Libre du Québec – Permissive version 1.1',
        'referenceNumber': 188
    },
    'LiLiQ-R-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'LiLiQ-R-1.1',
        'name': 'Licence Libre du Québec – Réciprocité version 1.1',
        'referenceNumber': 309
    },
    'LiLiQ-Rplus-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'LiLiQ-Rplus-1.1',
        'name': 'Licence Libre du Québec – Réciprocité forte '
        'version 1.1',
        'referenceNumber': 360
    },
    'Libpng': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Libpng',
        'name': 'libpng License',
        'referenceNumber': 404
    },
    'Linux-OpenIB': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Linux-OpenIB',
        'name': 'Linux Kernel Variant of OpenIB.org license',
        'referenceNumber': 228
    },
    'MIT': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'MIT',
        'name': 'MIT License',
        'referenceNumber': 271
    },
    'MIT-0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'MIT-0',
        'name': 'MIT No Attribution',
        'referenceNumber': 81
    },
    'MIT-CMU': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'MIT-CMU',
        'name': 'CMU License',
        'referenceNumber': 370
    },
    'MIT-advertising': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'MIT-advertising',
        'name': 'Enlightenment License (e16)',
        'referenceNumber': 203
    },
    'MIT-enna': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'MIT-enna',
        'name': 'enna License',
        'referenceNumber': 60
    },
    'MIT-feh': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'MIT-feh',
        'name': 'feh License',
        'referenceNumber': 392
    },
    'MIT-open-group': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'MIT-open-group',
        'name': 'MIT Open Group variant',
        'referenceNumber': 327
    },
    'MITNFA': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'MITNFA',
        'name': 'MIT +no-false-attribs license',
        'referenceNumber': 361
    },
    'MPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'MPL-1.0',
        'name': 'Mozilla Public License 1.0',
        'referenceNumber': 253
    },
    'MPL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'MPL-1.1',
        'name': 'Mozilla Public License 1.1',
        'referenceNumber': 432
    },
    'MPL-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'MPL-2.0',
        'name': 'Mozilla Public License 2.0',
        'referenceNumber': 136
    },
    'MPL-2.0-no-copyleft-exception': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'MPL-2.0-no-copyleft-exception',
        'name': 'Mozilla Public License 2.0 (no '
        'copyleft exception)',
        'referenceNumber': 193
    },
    'MS-PL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'MS-PL',
        'name': 'Microsoft Public License',
        'referenceNumber': 393
    },
    'MS-RL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'MS-RL',
        'name': 'Microsoft Reciprocal License',
        'referenceNumber': 4
    },
    'MTLL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'MTLL',
        'name': 'Matrix Template Library License',
        'referenceNumber': 103
    },
    'MakeIndex': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'MakeIndex',
        'name': 'MakeIndex License',
        'referenceNumber': 369
    },
    'MirOS': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'MirOS',
        'name': 'The MirOS Licence',
        'referenceNumber': 397
    },
    'Motosoto': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'Motosoto',
        'name': 'Motosoto License',
        'referenceNumber': 31
    },
    'MulanPSL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'MulanPSL-1.0',
        'name': 'Mulan Permissive Software License, Version 1',
        'referenceNumber': 214
    },
    'MulanPSL-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'MulanPSL-2.0',
        'name': 'Mulan Permissive Software License, Version 2',
        'referenceNumber': 152
    },
    'Multics': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'Multics',
        'name': 'Multics License',
        'referenceNumber': 245
    },
    'Mup': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Mup',
        'name': 'Mup License',
        'referenceNumber': 328
    },
    'NASA-1.3': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'NASA-1.3',
        'name': 'NASA Open Source Agreement 1.3',
        'referenceNumber': 126
    },
    'NBPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'NBPL-1.0',
        'name': 'Net Boolean Public License v1',
        'referenceNumber': 45
    },
    'NCGL-UK-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'NCGL-UK-2.0',
        'name': 'Non-Commercial Government Licence',
        'referenceNumber': 229
    },
    'NCSA': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'NCSA',
        'name': 'University of Illinois/NCSA Open Source License',
        'referenceNumber': 196
    },
    'NGPL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'NGPL',
        'name': 'Nethack General Public License',
        'referenceNumber': 332
    },
    'NIST-PD': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'NIST-PD',
        'name': 'NIST Public Domain Notice',
        'referenceNumber': 314
    },
    'NIST-PD-fallback': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'NIST-PD-fallback',
        'name': 'NIST Public Domain Notice with license fallback',
        'referenceNumber': 32
    },
    'NLOD-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'NLOD-1.0',
        'name': 'Norwegian Licence for Open Government Data',
        'referenceNumber': 146
    },
    'NLPL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'NLPL',
        'name': 'No Limit Public License',
        'referenceNumber': 329
    },
    'NOSL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'NOSL',
        'name': 'Netizen Open Source License',
        'referenceNumber': 408
    },
    'NPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'NPL-1.0',
        'name': 'Netscape Public License v1.0',
        'referenceNumber': 259
    },
    'NPL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'NPL-1.1',
        'name': 'Netscape Public License v1.1',
        'referenceNumber': 442
    },
    'NPOSL-3.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'NPOSL-3.0',
        'name': 'Non-Profit Open Software License 3.0',
        'referenceNumber': 154
    },
    'NRL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'NRL',
        'name': 'NRL License',
        'referenceNumber': 104
    },
    'NTP': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'NTP',
        'name': 'NTP License',
        'referenceNumber': 275
    },
    'NTP-0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'NTP-0',
        'name': 'NTP No Attribution',
        'referenceNumber': 195
    },
    'Naumen': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'Naumen',
        'name': 'Naumen Public License',
        'referenceNumber': 301
    },
    'Net-SNMP': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Net-SNMP',
        'name': 'Net-SNMP License',
        'referenceNumber': 296
    },
    'NetCDF': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'NetCDF',
        'name': 'NetCDF license',
        'referenceNumber': 225
    },
    'Newsletr': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Newsletr',
        'name': 'Newsletr License',
        'referenceNumber': 385
    },
    'Nokia': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'Nokia',
        'name': 'Nokia Open Source License',
        'referenceNumber': 127
    },
    'Noweb': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Noweb',
        'name': 'Noweb License',
        'referenceNumber': 70
    },
    'Nunit': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'Nunit',
        'name': 'Nunit License',
        'referenceNumber': 89
    },
    'O-UDA-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'O-UDA-1.0',
        'name': 'Open Use of Data Agreement v1.0',
        'referenceNumber': 43
    },
    'OCCT-PL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OCCT-PL',
        'name': 'Open CASCADE Technology Public License',
        'referenceNumber': 183
    },
    'OCLC-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'OCLC-2.0',
        'name': 'OCLC Research Public License 2.0',
        'referenceNumber': 367
    },
    'ODC-By-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'ODC-By-1.0',
        'name': 'Open Data Commons Attribution License v1.0',
        'referenceNumber': 409
    },
    'ODbL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'ODbL-1.0',
        'name': 'ODC Open Database License v1.0',
        'referenceNumber': 366
    },
    'OFL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'OFL-1.0',
        'name': 'SIL Open Font License 1.0',
        'referenceNumber': 83
    },
    'OFL-1.0-RFN': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OFL-1.0-RFN',
        'name': 'SIL Open Font License 1.0 with Reserved Font Name',
        'referenceNumber': 321
    },
    'OFL-1.0-no-RFN': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OFL-1.0-no-RFN',
        'name': 'SIL Open Font License 1.0 with no Reserved Font '
        'Name',
        'referenceNumber': 74
    },
    'OFL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'OFL-1.1',
        'name': 'SIL Open Font License 1.1',
        'referenceNumber': 335
    },
    'OFL-1.1-RFN': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'OFL-1.1-RFN',
        'name': 'SIL Open Font License 1.1 with Reserved Font Name',
        'referenceNumber': 42
    },
    'OFL-1.1-no-RFN': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'OFL-1.1-no-RFN',
        'name': 'SIL Open Font License 1.1 with no Reserved Font '
        'Name',
        'referenceNumber': 252
    },
    'OGC-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OGC-1.0',
        'name': 'OGC Software License, Version 1.0',
        'referenceNumber': 394
    },
    'OGL-Canada-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OGL-Canada-2.0',
        'name': 'Open Government Licence - Canada',
        'referenceNumber': 373
    },
    'OGL-UK-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OGL-UK-1.0',
        'name': 'Open Government Licence v1.0',
        'referenceNumber': 375
    },
    'OGL-UK-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OGL-UK-2.0',
        'name': 'Open Government Licence v2.0',
        'referenceNumber': 20
    },
    'OGL-UK-3.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OGL-UK-3.0',
        'name': 'Open Government Licence v3.0',
        'referenceNumber': 113
    },
    'OGTSL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'OGTSL',
        'name': 'Open Group Test Suite License',
        'referenceNumber': 25
    },
    'OLDAP-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-1.1',
        'name': 'Open LDAP Public License v1.1',
        'referenceNumber': 53
    },
    'OLDAP-1.2': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-1.2',
        'name': 'Open LDAP Public License v1.2',
        'referenceNumber': 46
    },
    'OLDAP-1.3': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-1.3',
        'name': 'Open LDAP Public License v1.3',
        'referenceNumber': 37
    },
    'OLDAP-1.4': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-1.4',
        'name': 'Open LDAP Public License v1.4',
        'referenceNumber': 150
    },
    'OLDAP-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-2.0',
        'name': 'Open LDAP Public License v2.0 (or possibly 2.0A and '
        '2.0B)',
        'referenceNumber': 21
    },
    'OLDAP-2.0.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-2.0.1',
        'name': 'Open LDAP Public License v2.0.1',
        'referenceNumber': 298
    },
    'OLDAP-2.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-2.1',
        'name': 'Open LDAP Public License v2.1',
        'referenceNumber': 430
    },
    'OLDAP-2.2': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-2.2',
        'name': 'Open LDAP Public License v2.2',
        'referenceNumber': 342
    },
    'OLDAP-2.2.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-2.2.1',
        'name': 'Open LDAP Public License v2.2.1',
        'referenceNumber': 412
    },
    'OLDAP-2.2.2': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-2.2.2',
        'name': 'Open LDAP Public License 2.2.2',
        'referenceNumber': 246
    },
    'OLDAP-2.3': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-2.3',
        'name': 'Open LDAP Public License v2.3',
        'referenceNumber': 441
    },
    'OLDAP-2.4': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-2.4',
        'name': 'Open LDAP Public License v2.4',
        'referenceNumber': 120
    },
    'OLDAP-2.5': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-2.5',
        'name': 'Open LDAP Public License v2.5',
        'referenceNumber': 110
    },
    'OLDAP-2.6': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-2.6',
        'name': 'Open LDAP Public License v2.6',
        'referenceNumber': 111
    },
    'OLDAP-2.7': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'OLDAP-2.7',
        'name': 'Open LDAP Public License v2.7',
        'referenceNumber': 242
    },
    'OLDAP-2.8': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'OLDAP-2.8',
        'name': 'Open LDAP Public License v2.8',
        'referenceNumber': 267
    },
    'OML': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OML',
        'name': 'Open Market License',
        'referenceNumber': 262
    },
    'OPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'OPL-1.0',
        'name': 'Open Public License v1.0',
        'referenceNumber': 364
    },
    'OSET-PL-2.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'OSET-PL-2.1',
        'name': 'OSET Public License version 2.1',
        'referenceNumber': 215
    },
    'OSL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'OSL-1.0',
        'name': 'Open Software License 1.0',
        'referenceNumber': 96
    },
    'OSL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'OSL-1.1',
        'name': 'Open Software License 1.1',
        'referenceNumber': 185
    },
    'OSL-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'OSL-2.0',
        'name': 'Open Software License 2.0',
        'referenceNumber': 398
    },
    'OSL-2.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'OSL-2.1',
        'name': 'Open Software License 2.1',
        'referenceNumber': 186
    },
    'OSL-3.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'OSL-3.0',
        'name': 'Open Software License 3.0',
        'referenceNumber': 156
    },
    'OpenSSL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'OpenSSL',
        'name': 'OpenSSL License',
        'referenceNumber': 84
    },
    'PDDL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'PDDL-1.0',
        'name': 'ODC Public Domain Dedication & License 1.0',
        'referenceNumber': 134
    },
    'PHP-3.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'PHP-3.0',
        'name': 'PHP License v3.0',
        'referenceNumber': 201
    },
    'PHP-3.01': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'PHP-3.01',
        'name': 'PHP License v3.01',
        'referenceNumber': 11
    },
    'PSF-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'PSF-2.0',
        'name': 'Python Software Foundation License 2.0',
        'referenceNumber': 93
    },
    'Parity-6.0.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Parity-6.0.0',
        'name': 'The Parity Public License 6.0.0',
        'referenceNumber': 436
    },
    'Parity-7.0.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Parity-7.0.0',
        'name': 'The Parity Public License 7.0.0',
        'referenceNumber': 423
    },
    'Plexus': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Plexus',
        'name': 'Plexus Classworlds License',
        'referenceNumber': 164
    },
    'PolyForm-Noncommercial-1.0.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'PolyForm-Noncommercial-1.0.0',
        'name': 'PolyForm Noncommercial License '
        '1.0.0',
        'referenceNumber': 300
    },
    'PolyForm-Small-Business-1.0.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'PolyForm-Small-Business-1.0.0',
        'name': 'PolyForm Small Business License '
        '1.0.0',
        'referenceNumber': 119
    },
    'PostgreSQL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'PostgreSQL',
        'name': 'PostgreSQL License',
        'referenceNumber': 9
    },
    'Python-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'Python-2.0',
        'name': 'Python License 2.0',
        'referenceNumber': 422
    },
    'QPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'QPL-1.0',
        'name': 'Q Public License 1.0',
        'referenceNumber': 308
    },
    'Qhull': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Qhull',
        'name': 'Qhull License',
        'referenceNumber': 130
    },
    'RHeCos-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'RHeCos-1.1',
        'name': 'Red Hat eCos Public License v1.1',
        'referenceNumber': 67
    },
    'RPL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'RPL-1.1',
        'name': 'Reciprocal Public License 1.1',
        'referenceNumber': 227
    },
    'RPL-1.5': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'RPL-1.5',
        'name': 'Reciprocal Public License 1.5',
        'referenceNumber': 106
    },
    'RPSL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'RPSL-1.0',
        'name': 'RealNetworks Public Source License v1.0',
        'referenceNumber': 56
    },
    'RSA-MD': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'RSA-MD',
        'name': 'RSA Message-Digest License',
        'referenceNumber': 306
    },
    'RSCPL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'RSCPL',
        'name': 'Ricoh Source Code Public License',
        'referenceNumber': 377
    },
    'Rdisc': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Rdisc',
        'name': 'Rdisc License',
        'referenceNumber': 347
    },
    'Ruby': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'Ruby',
        'name': 'Ruby License',
        'referenceNumber': 44
    },
    'SAX-PD': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SAX-PD',
        'name': 'Sax Public Domain Notice',
        'referenceNumber': 157
    },
    'SCEA': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SCEA',
        'name': 'SCEA Shared Source License',
        'referenceNumber': 139
    },
    'SGI-B-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SGI-B-1.0',
        'name': 'SGI Free Software License B v1.0',
        'referenceNumber': 198
    },
    'SGI-B-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SGI-B-1.1',
        'name': 'SGI Free Software License B v1.1',
        'referenceNumber': 307
    },
    'SGI-B-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'SGI-B-2.0',
        'name': 'SGI Free Software License B v2.0',
        'referenceNumber': 24
    },
    'SHL-0.5': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SHL-0.5',
        'name': 'Solderpad Hardware License v0.5',
        'referenceNumber': 52
    },
    'SHL-0.51': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SHL-0.51',
        'name': 'Solderpad Hardware License, Version 0.51',
        'referenceNumber': 302
    },
    'SISSL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'SISSL',
        'name': 'Sun Industry Standards Source License v1.1',
        'referenceNumber': 79
    },
    'SISSL-1.2': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SISSL-1.2',
        'name': 'Sun Industry Standards Source License v1.2',
        'referenceNumber': 62
    },
    'SMLNJ': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'SMLNJ',
        'name': 'Standard ML of New Jersey License',
        'referenceNumber': 230
    },
    'SMPPL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SMPPL',
        'name': 'Secure Messaging Protocol Public License',
        'referenceNumber': 108
    },
    'SNIA': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SNIA',
        'name': 'SNIA Public License 1.1',
        'referenceNumber': 326
    },
    'SPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'SPL-1.0',
        'name': 'Sun Public License v1.0',
        'referenceNumber': 263
    },
    'SSH-OpenSSH': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SSH-OpenSSH',
        'name': 'SSH OpenSSH license',
        'referenceNumber': 19
    },
    'SSH-short': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SSH-short',
        'name': 'SSH short notice',
        'referenceNumber': 68
    },
    'SSPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SSPL-1.0',
        'name': 'Server Side Public License, v 1',
        'referenceNumber': 359
    },
    'SWL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SWL',
        'name': 'Scheme Widget Library (SWL) Software License Agreement',
        'referenceNumber': 94
    },
    'Saxpath': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Saxpath',
        'name': 'Saxpath License',
        'referenceNumber': 28
    },
    'Sendmail': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Sendmail',
        'name': 'Sendmail License',
        'referenceNumber': 315
    },
    'Sendmail-8.23': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Sendmail-8.23',
        'name': 'Sendmail License 8.23',
        'referenceNumber': 323
    },
    'SimPL-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'SimPL-2.0',
        'name': 'Simple Public License 2.0',
        'referenceNumber': 265
    },
    'Sleepycat': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'Sleepycat',
        'name': 'Sleepycat License',
        'referenceNumber': 54
    },
    'Spencer-86': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Spencer-86',
        'name': 'Spencer License 86',
        'referenceNumber': 194
    },
    'Spencer-94': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Spencer-94',
        'name': 'Spencer License 94',
        'referenceNumber': 226
    },
    'Spencer-99': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Spencer-99',
        'name': 'Spencer License 99',
        'referenceNumber': 65
    },
    'StandardML-NJ': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'StandardML-NJ',
        'name': 'Standard ML of New Jersey License',
        'referenceNumber': 304
    },
    'SugarCRM-1.1.3': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'SugarCRM-1.1.3',
        'name': 'SugarCRM Public License v1.1.3',
        'referenceNumber': 368
    },
    'TAPR-OHL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'TAPR-OHL-1.0',
        'name': 'TAPR Open Hardware License v1.0',
        'referenceNumber': 3
    },
    'TCL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'TCL',
        'name': 'TCL/TK License',
        'referenceNumber': 58
    },
    'TCP-wrappers': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'TCP-wrappers',
        'name': 'TCP Wrappers License',
        'referenceNumber': 247
    },
    'TMate': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'TMate',
        'name': 'TMate Open Source License',
        'referenceNumber': 433
    },
    'TORQUE-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'TORQUE-1.1',
        'name': 'TORQUE v2.5+ Software License v1.1',
        'referenceNumber': 197
    },
    'TOSL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'TOSL',
        'name': 'Trusster Open Source License',
        'referenceNumber': 266
    },
    'TU-Berlin-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'TU-Berlin-1.0',
        'name': 'Technische Universitaet Berlin License 1.0',
        'referenceNumber': 400
    },
    'TU-Berlin-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'TU-Berlin-2.0',
        'name': 'Technische Universitaet Berlin License 2.0',
        'referenceNumber': 421
    },
    'UCL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'UCL-1.0',
        'name': 'Upstream Compatibility License v1.0',
        'referenceNumber': 310
    },
    'UPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'UPL-1.0',
        'name': 'Universal Permissive License v1.0',
        'referenceNumber': 147
    },
    'Unicode-DFS-2015': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Unicode-DFS-2015',
        'name': 'Unicode License Agreement - Data Files and '
        'Software (2015)',
        'referenceNumber': 282
    },
    'Unicode-DFS-2016': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'Unicode-DFS-2016',
        'name': 'Unicode License Agreement - Data Files and '
        'Software (2016)',
        'referenceNumber': 401
    },
    'Unicode-TOU': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Unicode-TOU',
        'name': 'Unicode Terms of Use',
        'referenceNumber': 15
    },
    'Unlicense': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'Unlicense',
        'name': 'The Unlicense',
        'referenceNumber': 180
    },
    'VOSTROM': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'VOSTROM',
        'name': 'VOSTROM Public License for Open Source',
        'referenceNumber': 379
    },
    'VSL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'VSL-1.0',
        'name': 'Vovida Software License v1.0',
        'referenceNumber': 420
    },
    'Vim': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'Vim',
        'name': 'Vim License',
        'referenceNumber': 217
    },
    'W3C': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'W3C',
        'name': 'W3C Software Notice and License (2002-12-31)',
        'referenceNumber': 109
    },
    'W3C-19980720': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'W3C-19980720',
        'name': 'W3C Software Notice and License (1998-07-20)',
        'referenceNumber': 283
    },
    'W3C-20150513': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'W3C-20150513',
        'name': 'W3C Software Notice and Document License '
        '(2015-05-13)',
        'referenceNumber': 112
    },
    'WTFPL': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'WTFPL',
        'name': 'Do What The F*ck You Want To Public License',
        'referenceNumber': 17
    },
    'Watcom-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'Watcom-1.0',
        'name': 'Sybase Open Watcom Public License 1.0',
        'referenceNumber': 233
    },
    'Wsuipa': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Wsuipa',
        'name': 'Wsuipa License',
        'referenceNumber': 273
    },
    'X11': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'X11',
        'name': 'X11 License',
        'referenceNumber': 102
    },
    'XFree86-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'XFree86-1.1',
        'name': 'XFree86 License 1.1',
        'referenceNumber': 159
    },
    'XSkat': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'XSkat',
        'name': 'XSkat License',
        'referenceNumber': 82
    },
    'Xerox': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Xerox',
        'name': 'Xerox License',
        'referenceNumber': 237
    },
    'Xnet': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': True,
        'licenseId': 'Xnet',
        'name': 'X.Net License',
        'referenceNumber': 362
    },
    'YPL-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'YPL-1.0',
        'name': 'Yahoo! Public License v1.0',
        'referenceNumber': 311
    },
    'YPL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'YPL-1.1',
        'name': 'Yahoo! Public License v1.1',
        'referenceNumber': 39
    },
    'ZPL-1.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'ZPL-1.1',
        'name': 'Zope Public License 1.1',
        'referenceNumber': 87
    },
    'ZPL-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'ZPL-2.0',
        'name': 'Zope Public License 2.0',
        'referenceNumber': 114
    },
    'ZPL-2.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'ZPL-2.1',
        'name': 'Zope Public License 2.1',
        'referenceNumber': 396
    },
    'Zed': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Zed',
        'name': 'Zed License',
        'referenceNumber': 115
    },
    'Zend-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'Zend-2.0',
        'name': 'Zend License v2.0',
        'referenceNumber': 416
    },
    'Zimbra-1.3': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'Zimbra-1.3',
        'name': 'Zimbra Public License v1.3',
        'referenceNumber': 206
    },
    'Zimbra-1.4': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'Zimbra-1.4',
        'name': 'Zimbra Public License v1.4',
        'referenceNumber': 413
    },
    'Zlib': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': True,
        'licenseId': 'Zlib',
        'name': 'zlib License',
        'referenceNumber': 41
    },
    'blessing': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'blessing',
        'name': 'SQLite Blessing',
        'referenceNumber': 444
    },
    'bzip2-1.0.5': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'bzip2-1.0.5',
        'name': 'bzip2 and libbzip2 License v1.0.5',
        'referenceNumber': 199
    },
    'bzip2-1.0.6': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'bzip2-1.0.6',
        'name': 'bzip2 and libbzip2 License v1.0.6',
        'referenceNumber': 69
    },
    'copyleft-next-0.3.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'copyleft-next-0.3.0',
        'name': 'copyleft-next 0.3.0',
        'referenceNumber': 344
    },
    'copyleft-next-0.3.1': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'copyleft-next-0.3.1',
        'name': 'copyleft-next 0.3.1',
        'referenceNumber': 406
    },
    'curl': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'curl',
        'name': 'curl License',
        'referenceNumber': 338
    },
    'diffmark': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'diffmark',
        'name': 'diffmark license',
        'referenceNumber': 427
    },
    'dvipdfm': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'dvipdfm',
        'name': 'dvipdfm License',
        'referenceNumber': 14
    },
    'eCos-2.0': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'eCos-2.0',
        'name': 'eCos license version 2.0',
        'referenceNumber': 290
    },
    'eGenix': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'eGenix',
        'name': 'eGenix.com Public License 1.1.0',
        'referenceNumber': 231
    },
    'etalab-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'etalab-2.0',
        'name': 'Etalab Open License 2.0',
        'referenceNumber': 276
    },
    'gSOAP-1.3b': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'gSOAP-1.3b',
        'name': 'gSOAP Public License v1.3b',
        'referenceNumber': 176
    },
    'gnuplot': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'gnuplot',
        'name': 'gnuplot License',
        'referenceNumber': 411
    },
    'iMatix': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'iMatix',
        'name': 'iMatix Standard Function Library Agreement',
        'referenceNumber': 184
    },
    'libpng-2.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'libpng-2.0',
        'name': 'PNG Reference Library version 2',
        'referenceNumber': 105
    },
    'libselinux-1.0': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'libselinux-1.0',
        'name': 'libselinux public domain notice',
        'referenceNumber': 12
    },
    'libtiff': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'libtiff',
        'name': 'libtiff License',
        'referenceNumber': 437
    },
    'mpich2': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'mpich2',
        'name': 'mpich2 License',
        'referenceNumber': 71
    },
    'psfrag': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'psfrag',
        'name': 'psfrag License',
        'referenceNumber': 438
    },
    'psutils': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'psutils',
        'name': 'psutils License',
        'referenceNumber': 294
    },
    'wxWindows': {
        'isDeprecatedLicenseId': True,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'wxWindows',
        'name': 'wxWindows Library License',
        'referenceNumber': 261
    },
    'xinetd': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': True,
        'isOsiApproved': False,
        'licenseId': 'xinetd',
        'name': 'xinetd License',
        'referenceNumber': 429
    },
    'xpp': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'xpp',
        'name': 'XPP License',
        'referenceNumber': 97
    },
    'zlib-acknowledgement': {
        'isDeprecatedLicenseId': False,
        'isFsfLibre': False,
        'isOsiApproved': False,
        'licenseId': 'zlib-acknowledgement',
        'name': 'zlib/libpng License with Acknowledgement',
        'referenceNumber': 260
    }
}
