# SPDX-FileCopyrightText: 2019 Michael Pöhn <michael@poehn.at>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import json
import pprint
import spdx_license_list
import pkgutil
import argparse
import pkg_resources
import urllib.request


VERSION = '0.5.2'


class PySpdxException(Exception):
    def __init__(self, code, msg):
        self.code = code
        self.msg = msg


def cmdprint(filter_fsf, filter_osi, filter_fsf_or_osi, show_deprecated):
    ids = []
    for license in spdx_license_list.LICENSES.values():
        if filter_fsf and not license['isFsfLibre']:
            continue
        if filter_osi and not license['isOsiApproved']:
            continue
        if filter_fsf_or_osi and not (license['isFsfLibre'] or license['isOsiApproved']):
            continue
        if not show_deprecated and license['isDeprecatedLicenseId']:
            continue
        ids.append(license['licenseId'])
    return pprint.pformat(ids, indent=4)


def get_latest_spdx_tag():
    url = 'https://api.github.com/repos/spdx/license-list-data/tags'
    with urllib.request.urlopen(url) as f:
        res = f.read().decode('utf-8')
    spdx_tags = json.loads(res)
    latest_tag = '0'

    for t in spdx_tags:
        if pkg_resources.parse_version(t['name']) > pkg_resources.parse_version(latest_tag):
            latest_tag = t['name']
    return latest_tag


def check_version():
    latest_spdx_tag = get_latest_spdx_tag()
    if pkg_resources.parse_version(latest_spdx_tag) != pkg_resources.parse_version(spdx_license_list.SPDX_GIT_TAG):
        raise PySpdxException(30,
                              "spdx_license_list not up to date. The newest SPDX "
                              "release is: '{new}'. This version of "
                              "spdx_license_list only ships with '{ours}'"
                              .format(new=latest_spdx_tag,
                                      ours=spdx_license_list.SPDX_GIT_TAG))
    else:
        return 'check successfull, {v} is the newest version'.format(v=spdx_license_list.SPDX_GIT_TAG)


def gen_py(ver):

    if 'yapf' not in [x.name for x in pkgutil.iter_modules()]:
        raise PySpdxException(41, 'error: this sub-command requires '
                              'yapf to be instaled\n'
                              '(eg. by running: python3 -m pip --user install yapf)')
    import yapf

    url = 'https://github.com/spdx/license-list-data/raw/{tag}/json/licenses.json'.format(tag=ver)
    with urllib.request.urlopen(url) as f:
        rawjson = json.loads(f.read().decode('utf-8'))
    licenses = {x['licenseId']: x for x in rawjson['licenses']}
    for license in licenses.values():
        del(license['detailsUrl'])
        del(license['reference'])
        del(license['seeAlso'])
        if 'isOsiApproved' not in license:
            license['isOsiApproved'] = False
        if 'isFsfLibre' not in license:
            license['isFsfLibre'] = False
        license['referenceNumber'] = int(license['referenceNumber'])
    raw_code = "#! /usr/bin/env python3\n" + \
               "\n" + \
               "# SPDX-#reusehatesme-FileCopyrightText: 2019 Michael Pöhn <michael@poehn.at>\n" + \
               "#\n" + \
               "# SPDX-#reusehatesme-License-Identifier: GPL-3.0-or-later\n" + \
               "\n" + \
               "# this file is generated using `python -m spdx_license_list gen-py`\n" + \
               "# do not modify it manually\n\n" + \
               "NAME = 'spdx_license_list'\n\n" + \
               "VERSION = '{v}'\n\n".format(v=VERSION) + \
               "SPDX_GIT_TAG = '{v}'\n\n".format(v=ver) + \
               "LICENSES = {}".format(pprint.pformat(licenses))
    raw_code = raw_code.replace('-#reusehatesme', '')
    return yapf.yapflib.yapf_api.FormatCode(raw_code, style_config='pep8')[0]


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(
        description="This is a simple tool for fetching SPDX definitions. "
                    "We use it for updating our license checks.")
    argparser.add_argument('--show-deprecated', action='store_true',
                           help="also print display deprecated license IDs")
    sub_argparser = argparser.add_subparsers(dest="subcmd",
                                             help="available actions")
    sub_print = sub_argparser.add_parser('print',
                                         help="Prints the entire list "
                                              "of SPDX licenseIDs in "
                                              "python-list style.")
    sub_print.add_argument('--filter-fsf', action='store_true',
                           default=False,
                           help="only list licenses approved to be "
                                "'free software' by FSF")
    sub_print.add_argument('--filter-osi', action='store_true',
                           default=False,
                           help="only list licenses approved to be "
                                "'open source' by OSI")
    sub_print.add_argument('--filter-fsf-or-osi', action='store_true',
                           default=False,
                           help="only list licenses approved to be "
                                "'free software' by FSF "
                                "or approved to be "
                                "'open source' by OSI")
    sub_print.add_argument('--show-deprecated', action='store_true',
                           default=False,
                           help='also print deprecated licenseIDs')
    sub_argparser.add_parser('check-version',
                             help="Check whether SPDX updated their "
                                  "list of standardized license "
                                  "definitions. (latest known release: "
                                  "{v})".format(v=spdx_license_list.SPDX_GIT_TAG))

    sub_gen = sub_argparser.add_parser('gen-py',
                                       help="Generate and print python "
                                            "dict from json SPDX data on"
                                            "https://github.com/spdx/license-list-data "
                                            "(mostly used for updating "
                                            "this project)")
    sub_gen.add_argument('--version', default=spdx_license_list.SPDX_GIT_TAG,
                         help="specify version (git-tag)")

    args = argparser.parse_args()

    try:
        if args.subcmd == 'print':
            print(cmdprint(args.filter_fsf,
                           args.filter_osi,
                           args.filter_fsf_or_osi,
                           args.show_deprecated))
        elif args.subcmd == 'check-version':
            print(check_version())
        elif args.subcmd == 'gen-py':
            print(gen_py(args.version), end='')
        else:
            print('error: please supply a sub command. (see -h for help)')
            sys.exit(2)
    except PySpdxException as e:
        print('error:', e.msg)
        sys.exit(e.code)
